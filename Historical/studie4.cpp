
//tabel:
//	dc.b 50,51,52,53,54,55,0
//
//destbuf:
//  blk.b 20,0
//
//main:
//  lea tabel,a0
//  lea destbuf,a1
//loop:
//  move.b(a0)+,(a1)+
//  cmp.b #0, -1(a0)
//  bne  loop
//  rts




#include "68000asm.h"
#include "studie4.h"

char LABEL_TABEL[] = {50,51,52,53,54,55,0};

char LABEL_DESTBUF[20];


void studie4_main()
{
	Init68000asm();

	//start point   (jmp main)
	State68000 = STATE_LABEL_MAIN;
	while(1)
	{
		switch(State68000)
		{
		case STATE_LABEL_MAIN:
			a0 = (int)LABEL_TABEL;

			a1 = (int)LABEL_DESTBUF;

			State68000 = STATE_LABEL_LOOP;
			break;

		case STATE_LABEL_LOOP:
			(*((unsigned char *)a1++)) = (*((unsigned char *)a0++));

			sr &= SR_ZEROFLAG;
			if(*((unsigned char *)(a0-1))==0) sr|=SR_ZEROFLAG;
			
			if(!(sr&SR_ZEROFLAG))
			{
				State68000 = STATE_LABEL_LOOP;
				break;
			}

			if(a7 == (int)(&Stack[MAXSTACKAMOUNT])) goto end;
			State68000 = *((int *)(a7++));
			break;
		default:
			assert(false);
			break;
		}
	}

end:
	int j;
	j=0;
}

