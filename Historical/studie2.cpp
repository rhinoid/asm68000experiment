//main:
//	move.l #0,d0
//	move.l #200,d7
//loop:
//	add.l  d7,d0
//	sub.l  #1,d7
//        cmp.l  #0,d7
//        bne    loop
//        rts
//

#include "68000asm.h"
#include "studie2.h"

void studie2_main()
{
	Init68000asm();

	//start point   (jmp main)
	State68000 = STATE_LABEL_MAIN;
	while(1)
	{
		switch(State68000)
		{
		case STATE_LABEL_MAIN:
			d0 = 0;

			d7 = 200;

			State68000 = STATE_LABEL_LOOP;
			break;
		case STATE_LABEL_LOOP:
			d0 += d7;
			TestZero(d0);

			d7--;
			TestZero(d7);

			sr &= SR_ZEROFLAG;
			if(d7==0) sr|=SR_ZEROFLAG;

			if(!(sr&SR_ZEROFLAG))
			{
				State68000 = STATE_LABEL_LOOP;
				break;
			}

			if(a7 == (int)(&Stack[MAXSTACKAMOUNT])) goto end;
			State68000 = *((int *)(a7++));
			break;
		default:
			assert(false);
		}
	}

end:
	int j;
	j=0;
}

