
//jumptabel:
//	dc.l floep, floep2
//
//main:
//  moveq #0,d0
//  lea   jumptabel,a0
//  jsr   (a0,d0)
//	rts
//
//floep:
//	move.l #0,d1
//  rts
//
//floep2:
//	move.l #1,d1
//  rts
//


#include "68000asm.h"
#include "studie5.h"

int LABEL_JUMPTABEL[] = {STATE_LABEL_FLOEP, STATE_LABEL_FLOEP2};



void studie5_main()
{
	Init68000asm();

	//start point   (jmp main)
	State68000 = STATE_LABEL_MAIN;
	while(1)
	{
		switch(State68000)
		{
		case STATE_LABEL_MAIN:
			d0 = 1;
			a0 = (int)LABEL_JUMPTABEL;

			*((int *)(--a7)) = STATE_RETURNLABEL_1;
			State68000 = *((unsigned long *)a0+d0);
			break;

		case STATE_RETURNLABEL_1:
			if(a7 == (int)(&Stack[MAXSTACKAMOUNT])) goto end;
			State68000 = *((int *)(a7++));
			break;

		case STATE_LABEL_FLOEP:
			d1 = 0;
			if(a7 == (int)(&Stack[MAXSTACKAMOUNT])) goto end;
			State68000 = *((int *)(a7++));
			break;

		case STATE_LABEL_FLOEP2:
			d1 = 1;

			if(a7 == (int)(&Stack[MAXSTACKAMOUNT])) goto end;
			State68000 = *((int *)(a7++));
			break;

		default:
			assert(false);
			break;
		}
	}

end:
	int j;
	j=0;
}

