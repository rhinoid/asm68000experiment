//main:
//	move.l #5,d0
//	move.l #4,d1
//	jsr    subtract
//  rts
//
//subtract:
//	sub.l d1,d0
//	rts




#include "68000asm.h"
#include "studie3.h"

void studie3_main()
{
	Init68000asm();

	//start point   (jmp main)
	State68000 = STATE_LABEL_MAIN;
	while(1)
	{
		switch(State68000)
		{
		case STATE_LABEL_MAIN:
			d0 = 5;

			d1 = 4;

			*((int *)(--a7)) = STATE_RETURNLABEL_1;
			State68000 = STATE_LABEL_SUBTRACT;
			break;

		case STATE_RETURNLABEL_1:
			if(a7 == (int)(&Stack[MAXSTACKAMOUNT])) goto end;
			State68000 = *((int *)(a7++));
			break;

		case STATE_LABEL_SUBTRACT:
			d0 -= d1;
			
			if(a7 == (int)(&Stack+MAXSTACKAMOUNT)) goto end;
			State68000 = *((int *)(a7++));
			break;
		default:
			assert(false);
			break;
		}
	}

end:
	int j;
	j=0;
}

