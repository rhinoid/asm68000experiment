
//testtab1:
//	dc.b	
//
//LongTellertje: dc.l 0
//WordTellertje: dc.w 0
//ByteTellertje: dc.b 0
//               dc.b 0    ;padding
//text: dc.b ' dit is een text'
//      dc.b 'welke blij verder gaat op de volgende regel'
//
//emptybuf: blk.b 0
//
//crsr:
//	dc.w	%0111111100000000,%1000000010000000
//	dc.w	%1000000010000000,%0000000000000000
//	dc.w	%1000000010000000,%0000000000000000
//	dc.w	%1000000010000000,%0000000000000000
//	dc.w	%1000000010000000,%0000000000000000
//	dc.w	%1000000010000000,%0000000000000000
//	dc.w	%1000000010000000,%0000000000000000
//	dc.w	%1000000010000000,%0000000000000000
//	dc.w	%0111111100000000,%1000000010000000
//	dc.w	%0000000000000000,%0000000000000000
//	dc.w	%0000000000000000,%0000000000000000
//
//	blk.l	8,0
//
//oldx:	dc.b	0
//oldy:	dc.b	0
// 
//jumptabi:
//	dc.l	dwnsmple,upsmple,scrl,scrr,flipx,flipy 
//
//chandats:
//	dc.l	datach1,$dff0a0
//	dc.w	$0001
//	dc.l	datach2,$dff0b0
//	dc.w	$0002
//	dc.l	datach3,$dff0c0
//	dc.w	$0004
//	dc.l	datach4,$dff0d0
//	dc.w	$0008 
//datach1:	blk.b	48,0
//datach2:	blk.b	48,0
//datach3:	blk.b	48,0
//datach4:	blk.b	48,0
 


#include "68000asm.h"
#include "studie6.h"

void studie6_main()
{
	Init68000asm();

}


