This is an old Experiment/studie where I try to see how far I can push the boundaries of a C++ compiler 
and make it accept a syntax as similar to the old 68000 assembly syntax.

I was hoping that this might prove a great way to recompile old Amiga games I have into more portable code.

In any case, it's interesting code as far as I got it to work.

I corrected the code to make it compile under LLVM (It was made years ago in MSVC). Still works, although I think we should silently store this away under compiler torture.