
#include "68000asm.h"
#include "studie7.h"


START68000

EQU(offset,1)

DATA(tabel1)
	DCW (0x1234)
	DCB (0x56)
	DCB (0x78)
	DCL (0x87654321)
	DCL (0x11335577)
ENDDATA(tabel1)


LABEL(main)
	LEA   (tabel1, a0)	//tabel indexing
	MOVEL (#offset, d0)
	MOVEL (1(a0,d0),d1)

	ADDW  (#$7000,d1)	// word operations on long registers
	ADDW  (#$7000,d1)
	SUBW  (#$7000,d1)
	SUBW  (#$7000,d1)

	
	MOVEW (#4,d1)
	BNE (skipover)   // Branches
	ADDL (#1,d0)
LABEL(skipover)     // mid routine labels

	JSR (subtract)	// sub routines

	MOVEL (#4,d0)
	JSR (stop)
	MOVEL (#2,d0)
	RTS

// simple example subroutine
LABEL(subtract)
	SUBL (d0,d1)
	RTS


// advanced subroutine where we push a new return adress on the stack. The rts will then go there instead of returning
LABEL(stop)
	PEA (stop2)
	RTS


LABEL(stop2)
	ADDL (#4,a7)	// Popping one return address of the stack... the next rts will now actually return to the caller of main (e.g. exit the program) rather then returning.
	RTS




LABEL(looptest)
	MOVEL (#4,d4)
	MOVEL (#0,d5)
LABEL(loop)
	SUBL (#1,d4)
	BNE (loop)
	RTS


END68000