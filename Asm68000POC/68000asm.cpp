#include "68000asm.h"

int d[8]; //data registers
int a[8]; //address registers

int sr;   //status register

int Stack[MAXSTACKAMOUNT]; //stack

STRING2INT Constants; //place where all constants are stored in


FuncPtr NextFunc;
int NestCount;

void Init68000asm(void)
{
	memset(d, 0, 8*4);
	memset(a, 0, 8*4);

	a[7] =  ((int)(&Stack[MAXSTACKAMOUNT]));
	NestCount = 0;

	Start68000();
}


FuncPtr Run68000asm(FuncPtr f)
{
	int StackTop = a[7]; // if we go through the stacktop we exit (in case we were called recursively)
	NextFunc = f;
	NestCount++;
	while(true)
	{
		NextFunc = (FuncPtr) NextFunc();
		if(NextFunc==0) //get address of stack
		{
			if(a[7] == (int)(&Stack[MAXSTACKAMOUNT])) \
			{
				break;
			}
			NextFunc = *((FuncPtr *)a[7]);
			a[7]+=4;
		}
		if(a[7]>StackTop)
		{
			NestCount--;
			return NextFunc;
		}
		if(NextFunc==0) break;
	}
	NestCount--;
	return 0;
}






