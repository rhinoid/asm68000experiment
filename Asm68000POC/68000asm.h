
#ifndef __68000ASM_H
#define __68000ASM_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#pragma warning(disable:4786)
#include <string>
#include <map>

using namespace std;
typedef map<string, int, less<string> > STRING2INT;


///////////////////////////////////////////////////
//TYPEDEFS
///////////////////////////////////////////////////
typedef unsigned int UINT32;
typedef unsigned short UINT16;
typedef unsigned char UINT8;


///////////////////////////////////////////////////
//PROTOTYPES
///////////////////////////////////////////////////
typedef void *(*FuncPtr)();
void Init68000asm(void); //init asm
void *Start68000();		 //contains all equ's
FuncPtr Run68000asm(FuncPtr f); //jump to label main
void AddEqu(char *Name, int Value);

void subMOVEL(char *regs, char *regd);
void subMOVEW(char *regs, char *regd);
void subMOVEB(char *regs, char *regd);
void subADDL(char *regs, char *regd);
void subADDW(char *regs, char *regd);
void subADDB(char *regs, char *regd);
void subSUBL(char *regs, char *regd);
void subSUBW(char *regs, char *regd);


///////////////////////////////////////////////////
//REGISTERS
///////////////////////////////////////////////////
extern int d[8];
extern int a[8];


///////////////////////////////////////////////////
//LABEL DECLARATIONS
///////////////////////////////////////////////////
#define START68000 void *Start68000(){
#define END68000 }

//declare a label
#define FUNCTIONDEC(x) void *label_##x();

//Label
#define LABEL(x) JMP(x)} void *label_##x(){


///////////////////////////////////////////////////
//DATA DECLARATIONS
///////////////////////////////////////////////////
#define DATA(x) return 0; } UINT8 label_##x[] = {
#define DCB(a)  ((UINT8)(a)),
#define DCW(a)  ((UINT16)(a))&0xff,(((UINT16)(a))>>8)&0xff,
#define DCL(a)  ((UINT32)(a))&0xff,(((UINT32)(a))>>8)&0xff,(((UINT32)(a))>>16)&0xff,(((UINT32)(a))>>24)&0xff,
#define ENDDATA(x) }; void *dummylabel_##x(){


///////////////////////////////////////////////////
//CONSTANTS
///////////////////////////////////////////////////
extern STRING2INT Constants; //place where all equ's(constants) are stored in
#define EQU(name,value) Constants.insert(STRING2INT::value_type(#name,value));


///////////////////////////////////////////////////
//OPERAND
///////////////////////////////////////////////////
#define PARSEOPERAND(x, value, pvalue, index, postincrflg) \
{\
	char *str=x; \
	if(str[0]=='#'){str++; value = ScanNumber(&str); postincrflg = index = 0;}\
	else if(str[0]=='a'&&str[2]==0){ postincrflg = index = 0; pvalue = (curtype *) &a[str[1]-0x30]; value = *(pvalue); }\
	else if(str[0]=='d'&&str[2]==0){ postincrflg = index = 0; pvalue = (curtype *) &d[str[1]-0x30]; value = *(pvalue); }\
	else if(str[0]=='-'){ postincrflg = 0; index = str[3]-0x30; a[index] -= sizeof(curtype); pvalue = (curtype *) a[index]; value = *(pvalue); }\
	else \
	{ \
		int offset=0; \
		if(str[0]!='(') offset = ScanNumber(&str); \
		if(str[0]=='(') \
		{ \
			if(str[3]==',')\
			{\
				index = str[2]-0x30; pvalue = (curtype *) (a[index]); pvalue += offset; pvalue += d[str[5]-0x30]; value = *(pvalue); postincrflg = 0; \
			}\
			else\
			{\
				index = str[2]-0x30; pvalue = (curtype *) a[index]; pvalue += offset; value = *(pvalue); postincrflg = (str[4]=='+') ? sizeof(curtype) : 0; \
			}\
		}\
	}\
}

//only recognize adres registers
#define PARSEOPERANDAREG(x, value, pvalue) \
{\
	char *str=x; \
	pvalue = (curtype *) &a[str[1]-0x30]; value = *(pvalue); \
}

//parses a decimal or hexadecimal number OR constant and returns its value. also progresses the char* to the next unparsed character
inline int ScanNumber(char **Buf)
{
	char *BufPtr;
	int Idx;
	int Number;
	Number=0;
	Idx = 0;
	BufPtr =*Buf;
	if(BufPtr[0]=='$') //hex number?
	{
		Idx++;
		while((BufPtr[Idx]>='0'&&BufPtr[Idx]<='9')|(BufPtr[Idx]>='a'&&BufPtr[Idx]<='f'))
		{
			int Digit;
			Digit = BufPtr[Idx];
			Digit -= (Digit < 'a') ? '0' : ('a'-10);
			Number <<=4;
			Number += Digit;
			Idx++;
		}
	}
	else	//decimal number
	{
		if(BufPtr[0]>='0'&&BufPtr[0]<='9') //decimal number
		{
			while((BufPtr[Idx]>='0'&&BufPtr[Idx]<='9'))
			{
				int Digit;
				Digit = BufPtr[Idx];
				Digit -= '0';
				Number *= 10;
				Number += Digit;
				Idx++;
			}
		}
		else //a constant
		{
			STRING2INT::iterator It;
			char buffer[64];
			while((BufPtr[Idx]>='a'&&BufPtr[Idx]<='z')||(BufPtr[Idx]>='A'&&BufPtr[Idx]<='Z')||BufPtr[Idx]=='_')
			{
				buffer[Idx]=BufPtr[Idx];
				Idx++;
			}
			buffer[Idx]=0;
            It = Constants.find(buffer);
            if(It != Constants.end() ) Number = (*It).second;
            else assert(0);   // constant is not defined!
		}
	}
	*Buf = &BufPtr[Idx];
	return Number;
}


//increment the adressregister if necessary
#define POSTINCR(index, postincrflg) a[index]+=postincrflg


///////////////////////////////////////////////////
//STATUS REGISTER
///////////////////////////////////////////////////
extern int sr;

#define SR_ZEROFLAG (1)
#define SR_OVERFLOW (2)
#define SR_EXTENDED (4)
#define SR_NEGATIVE (8)
#define SR_CARRY    (16)

#define TESTZERO(x) sr &= ~SR_ZEROFLAG; if (x==0)sr |= SR_ZEROFLAG
#define TESTNEG(x) sr &= ~SR_NEGATIVE; if (x<0)sr |= SR_NEGATIVE
#define TESTCARRY(x) sr &= ~SR_CARRY; if(x) sr |= SR_CARRY
#define CLEARCARRY sr &= ~SR_CARRY
#define CLEAROVERFLOW sr &= ~SR_OVERFLOW


///////////////////////////////////////////////////
//STACK (where a7 points to)
///////////////////////////////////////////////////
#define MAXSTACKAMOUNT (1024)
extern int Stack[MAXSTACKAMOUNT];


///////////////////////////////////////////////////
//FLOW CONTROL
///////////////////////////////////////////////////
#define JMP(f) { return((void *)label_##f); }

#define JSR(f) \
{ \
	int ThisRetAddr = (__LINE__ | 0x80000000); \
	FuncPtr RealRetAddr; \
	a[7] -= 4; \
    *((int *)a[7]) = ThisRetAddr; \
	RealRetAddr = Run68000asm(label_##f); \
    if(RealRetAddr != (FuncPtr) ThisRetAddr) return (void *)RealRetAddr; \
}

#define RTS \
{ \
	return 0; \
}

// branches
#define BNE(x) { if((sr&SR_ZEROFLAG) != SR_ZEROFLAG) {JMP(x)}}




///////////////////////////////////////////////////
//MOVE COMMAND
///////////////////////////////////////////////////
//#define MOVEL(regs,regd) subMOVEL(#regs,#regd);
//#define MOVEW(regs,regd) subMOVEW(#regs,#regd);
//#define MOVEB(regs,regd) subMOVEB(#regs,#regd);
#define MOVEL(regs,regd) subMOVE((char *)#regs,(char *)#regd, (UINT32) 0);
#define MOVEW(regs,regd) subMOVE((char *)#regs,(char *)#regd, (UINT16) 0);
#define MOVEB(regs,regd) subMOVE((char *)#regs,(char *)#regd, (UINT8) 0);


///////////////////////////////////////////////////
//ARITHMETIC COMMANDS
///////////////////////////////////////////////////
#define ADDL(regs,regd) subADD((char *)#regs,(char *)#regd, (UINT32) 0);
#define ADDW(regs,regd) subADD((char *)#regs,(char *)#regd, (UINT16) 0);
#define ADDB(regs,regd) subADD((char *)#regs,(char *)#regd, (UINT8) 0);
#define SUBL(regs,regd) subSUB((char *)#regs,(char *)#regd, (UINT32) 0);
#define SUBW(regs,regd) subSUB((char *)#regs,(char *)#regd, (UINT16) 0);
#define SUBB(regs,regd) subSUB((char *)#regs,(char *)#regd, (UINT8) 0);


//**** SUBL(regs,regd) ****  (sub.l d0,d1) -=> subtract d0 from d1


///////////////////////////////////////////////////
//ADDRESS COMMANDS
///////////////////////////////////////////////////
#define PEA(f) \
{ \
	a[7] -= 4; \
	*((FuncPtr *)a[7]) = label_##f; \
};

#define LEA(addr,regd) { typedef UINT32 curtype; curtype dst,*pdst; PARSEOPERANDAREG((char *)#regd, dst, pdst); *pdst=(int)label_##addr;}



///////////////////////////////////////////////////
//TEMPLATED IMPLEMENTATIONS
///////////////////////////////////////////////////

template <class curtype>
void subMOVE(char *regs, char *regd, curtype size)
{ 
	curtype src,dst,*psrc,*pdst; 
	int sidx,didx,sflg,dflg; 
	PARSEOPERAND(regs, src, psrc, sidx, sflg); 
	PARSEOPERAND(regd, dst, pdst, didx, dflg); 
	*pdst=src; 
	POSTINCR(sidx, sflg); 
	POSTINCR(didx, dflg); 
	TESTZERO(src); 
	TESTNEG(src); 
	CLEARCARRY; 
	CLEAROVERFLOW;
}

template <class curtype>
void subADD(char *regs, char *regd, curtype size)
{ 
	curtype src,dst,olddst, *psrc,*pdst; 
	int sidx,didx,sflg,dflg; 
	PARSEOPERAND(regs, src, psrc, sidx, sflg); 
	PARSEOPERAND(regd, dst, pdst, didx, dflg); 
	olddst=dst; 
	dst+=src; 
	*pdst=dst; 
	POSTINCR(sidx, sflg); 
	POSTINCR(didx, dflg); 
	TESTZERO(src); 
	TESTNEG(src); 
	CLEARCARRY; 
	CLEAROVERFLOW;
}

template <class curtype>
void subSUB(char *regs, char *regd, curtype size)
{
	curtype src,dst,olddst,*psrc,*pdst; 
	int sidx,didx,sflg,dflg; 
	PARSEOPERAND(regs, src, psrc, sidx, sflg); 
	PARSEOPERAND(regd, dst, pdst, didx, dflg); 
	olddst=dst; 
	dst-=src; 
	*pdst=dst; 
	TESTCARRY(dst>olddst); 
	TESTZERO(src); 
	TESTNEG(src); 
	CLEAROVERFLOW;
}

#endif
