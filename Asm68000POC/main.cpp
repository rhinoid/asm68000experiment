//
//  main.cpp
//  Asm68000POC
//
//  Created by reinier van vliet on 25/9/15.
//  Copyright © 2015 Proof of Concept. All rights reserved.
//

#include <iostream>
#include "68000asm.h"
#include "studie7.h"

int main(int argc, const char * argv[]) {
    Init68000asm();
    Run68000asm(label_main);
    return 0;
}
